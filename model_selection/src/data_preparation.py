import glob

import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

from src.core.points import Points
from src.core.result import Result
from src.core.team import Team


def get_data():
    path = "../resource/*.csv"
    all_files = glob.glob(path)

    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    dataset = pd.concat(li, axis=0, ignore_index=True)
    dataset = dataset.sort_values(by=['event_timestamp'])

    return dataset.drop(
        ['fixture_id', 'league_id', 'league_name', 'league_country', 'league_logo', 'league_flag', 'event_date',
         'event_timestamp', 'firstHalfStart', 'secondHalfStart', 'round', 'status', 'statusShort', 'elapsed',
         'venue', 'referee', 'homeTeam_logo', 'awayTeam_logo',
         'score_halftime', 'score_fulltime', 'score_extratime', 'score_penalty'], axis='columns')


def make_results(dataset):
    team_first_goals = dataset['goalsHomeTeam']
    team_second_goals = dataset['goalsAwayTeam']
    results = []

    for i in range(len(team_first_goals)):
        result = get_result(team_second_goals[i], team_first_goals[i])
        results.append(result)

    return dataset.join(pd.DataFrame({'result': results}))


def get_x(dataset):
    return dataset.drop(['homeTeam_team_name', 'awayTeam_team_name', 'goalsHomeTeam', 'goalsAwayTeam', 'result'],
                        axis='columns')


def get_y(dataset):
    return dataset['result']


def get_neural_network_y(dataset):
    y = dataset['result'].values
    y = y.reshape(-1, 1)
    encoder = OneHotEncoder(sparse=False)
    one_hot_y = encoder.fit_transform(y)
    return one_hot_y


def get_results_with_last_matches_info(dataset, teams, number_of_last_matches):
    team_first_goals = dataset['goalsHomeTeam']
    team_second_goals = dataset['goalsAwayTeam']
    team_firsts_id = dataset['homeTeam_team_id']
    team_seconds_id = dataset['awayTeam_team_id']

    results = []
    last_matches_info = []
    param_size = 6

    initialize_last_matches_info(last_matches_info, number_of_last_matches, param_size)

    for i in range(len(dataset)):
        team_first = get_team(teams, team_firsts_id[i])
        team_second = get_team(teams, team_seconds_id[i])

        if not is_info_about_last_matches_of_teams(team_first, team_second, number_of_last_matches):
            initialize_array(number_of_last_matches, param_size, last_matches_info)
        else:
            set_past_goals_and_results_of_past_matches(team_second, team_first, number_of_last_matches, param_size,
                                                       last_matches_info)

        result = get_result_and_save_to_team(team_second, team_second_goals[i], team_first, team_first_goals[i])
        actualize_teams_info(team_second, team_second_goals[i], team_first, team_first_goals[i])

        results.append(int(result))

    dataset = append_last_matches_info_to_dataset(dataset, last_matches_info, number_of_last_matches, param_size,
                                                  results)

    return dataset.dropna(subset=['1_team_first_goals_for'])


def get_dataset_with_matches_host(dataset):
    dataset = make_team_first_host(dataset)
    dataset = make_team_second_host(dataset)
    return dataset


def get_result(team_second_goals, team_first_goals):
    if team_first_goals > team_second_goals:
        return 1
    if team_first_goals < team_second_goals:
        return 2
    if team_first_goals == team_second_goals:
        return 0


def actualize_teams_info(team_second, team_second_goals, team_first, team_first_goals):
    team_first.goals_for.append(team_first_goals)
    team_first.goals_against.append(team_second_goals)
    team_second.goals_for.append(team_second_goals)
    team_second.goals_against.append(team_first_goals)


def append_last_matches_info_to_dataset(dataset, last_matches_info, number_of_last_matches, param_size, results):
    data = {}
    for i in range(number_of_last_matches):
        j = i * param_size
        data.update({(str(i + 1) + "_team_first_goals_for"): last_matches_info[j]})
        data.update({(str(i + 1) + "_team_first_goals_against"): last_matches_info[j + 1]})
        data.update({(str(i + 1) + "_team_first_past_result"): last_matches_info[j + 2]})
        data.update({(str(i + 1) + "_team_second_goals_for"): last_matches_info[j + 3]})
        data.update({(str(i + 1) + "_team_second_goals_against"): last_matches_info[j + 4]})
        data.update({(str(i + 1) + "_team_second_past_result"): last_matches_info[j + 5]})
    data.update({"result": results})
    dataset = dataset.join(pd.DataFrame(data))
    return dataset


def initialize_last_matches_info(last_matches_info, number_of_last_matches, param_size):
    for i in range(param_size * number_of_last_matches):
        last_matches_info.append([])


def get_result_and_save_to_team(team_second, team_second_goals, team_first, team_first_goals):
    if team_first_goals > team_second_goals:
        team_first.results.append(int(Points.WIN))
        team_second.results.append(int(Points.DEFEAT))
        return Result.HOME_WIN
    if team_first_goals < team_second_goals:
        team_first.results.append(int(Points.DEFEAT))
        team_second.results.append(int(Points.WIN))
        return Result.AWAY_WIN
    if team_first_goals == team_second_goals:
        team_first.results.append(int(Points.DRAW))
        team_second.results.append(int(Points.DRAW))
        return Result.DRAW


def set_past_goals_and_results_of_past_matches(team_second, team_first, number_of_last_matches, param_size, tab):
    for k in range(number_of_last_matches):
        j = k * param_size
        position = -(k + 1)

        tab[j].append(team_first.goals_for[position])
        tab[j + 1].append(team_first.goals_against[position])
        tab[j + 2].append(team_first.results[position])

        tab[j + 3].append(team_second.goals_for[position])
        tab[j + 4].append(team_second.goals_against[position])
        tab[j + 5].append(team_second.results[position])


def initialize_array(number_of_last_matches, param_size, tab):
    for k in range(number_of_last_matches):
        for j in range(param_size):
            pos = k * param_size + j
            tab[pos].append(np.nan)


def is_info_about_last_matches_of_teams(team_first, team_second, number_of_last_matches):
    return len(team_first.results) >= number_of_last_matches and len(team_second.results) >= number_of_last_matches


def get_team(teams, id):
    for team in teams:
        if team.id == id:
            return team


def make_team_first_host(dataset):
    host = np.ones(len(dataset))
    return dataset.join(pd.DataFrame({'host': host}))


def make_team_second_host(dataset):
    matches_with_team_second_host = []
    initialize_matches_with_host(matches_with_team_second_host)
    new_result = None
    new_host = 2

    for index, match in dataset.iterrows():
        team_first_id = match['homeTeam_team_id']
        team_second_id = match['awayTeam_team_id']
        team_first_goals = match['goalsHomeTeam']
        team_second_goals = match['goalsAwayTeam']
        result = match['result']

        new_result = get_new_host_result(new_result, result)

        matches_with_team_second_host[0].append(team_second_id)
        matches_with_team_second_host[1].append(team_first_id)
        matches_with_team_second_host[2].append(team_second_goals)
        matches_with_team_second_host[3].append(team_first_goals)
        matches_with_team_second_host[4].append(new_result)
        matches_with_team_second_host[5].append(new_host)

    new_host_dataset = get_new_host_dataset(matches_with_team_second_host)

    return dataset.append(new_host_dataset)


def get_new_host_dataset(matches_with_team_second_host):
    new_host_dataset = pd.DataFrame({
        'homeTeam_team_id': matches_with_team_second_host[0],
        'awayTeam_team_id': matches_with_team_second_host[1],
        'goalsHomeTeam': matches_with_team_second_host[2],
        'goalsAwayTeam': matches_with_team_second_host[3],
        'result': matches_with_team_second_host[4],
        'host': matches_with_team_second_host[5]
    })
    return new_host_dataset


def get_new_host_result(new_result, result):
    if result == 0:
        new_result = 0
    if result == 1:
        new_result = 2
    if result == 2:
        new_result = 1
    return new_result


def initialize_matches_with_host(matches_with_team_second_host):
    for i in range(6):
        matches_with_team_second_host.append([])


def get_teams(dataset):
    team_ids = dataset['homeTeam_team_id'].unique()
    team_names = dataset['homeTeam_team_name'].unique()

    teams = []

    numbers_of_teams = len(team_names)
    if numbers_of_teams == len(team_ids):
        for i in range(numbers_of_teams):
            team = Team(team_ids[i], team_names[i])
            teams.append(team)
    return teams
