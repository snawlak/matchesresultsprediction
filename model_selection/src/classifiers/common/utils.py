from sklearn.metrics import accuracy_score


def get_score(classifier, X_train, y_train, X_test, y_test):
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    return accuracy_score(y_test, y_pred)


def show_msg(classifier_name, train_best_score, test_score, best_params):
    print(classifier_name + '***********************')
    print(classifier_name + " train + validation acc: " + str(train_best_score))
    print(classifier_name + " test acc: " + str(test_score))
    print(classifier_name + " best params: " + str(best_params) + "\n")


def show_neural_network_msg(acc_test, acc_val, history):
    print("history.history['accuracy']" + str(acc_val))
    print(history.history.keys())
    print(str(acc_val))
    print(str(acc_test) + "\n")
