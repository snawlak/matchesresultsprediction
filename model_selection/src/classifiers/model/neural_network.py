import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential

from src.classifiers.common.utils import show_neural_network_msg
from src.data_preparation import get_neural_network_y, get_x


def neural_network(dataset):
    X = get_x(dataset)
    y = get_neural_network_y(dataset)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    run_nn(X_train, y_train, X_test, y_test)


def run_nn(X, y, x_test, y_test):
    epochs = 15
    layer_size1 = 128
    layer_size2 = 256
    layer_size3 = 64
    act = 'relu'

    model = create_model(layer_size1, layer_size2, layer_size3, act)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    history = model.fit(X, y, validation_split=0.2, epochs=epochs, batch_size=32)
    acc_val = history.history['accuracy'][-1]
    test_loss, acc_test = model.evaluate(x_test, y_test)

    show_neural_network_msg(acc_test, acc_val, history)

    plot_model(history)


def create_model(layer_size1, layer_size2, layer_size3, act):
    model = Sequential()
    model.add(Dense(layer_size1, input_dim=2, activation='relu'))
    if layer_size2 > 0:
        model.add(Dense(layer_size2, activation='relu'))
    if layer_size3 > 0:
        model.add(Dense(layer_size3, activation='relu'))
    model.add(Dense(3, activation='softmax'))
    return model


def plot_model(history):
    summarize_history_for_accuracy(history)
    summarize_history_for_loss(history)


def summarize_history_for_loss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='lower right')
    plt.show()


def summarize_history_for_accuracy(history):
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='lower right')
    plt.show()
