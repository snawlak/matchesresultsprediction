from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

from src.classifiers.common.utils import get_score, show_msg


def random_forrest(X_train, y_train, X_test, y_test):
    classifier = RandomForestClassifier()
    param_grid = get_param_grid()

    grid = GridSearchCV(classifier, param_grid, cv=5, scoring='accuracy')

    grid.fit(X_train, y_train)
    best_params = grid.best_params_
    train_best_score = grid.best_score_

    random_forest_classifier = make_classifier_with_best_params(best_params)

    test_score = get_score(random_forest_classifier, X_train, y_train, X_test, y_test)

    show_msg("Random Forrest", train_best_score, test_score, best_params)


def get_param_grid():
    return {
        'n_estimators': [175, 190, 200, 225, 235, 250],
        'max_features': ['auto'],
        'max_depth': [8, 9, 10, 11, 12, 13, 14],
        'criterion': ['gini']
    }


def make_classifier_with_best_params(best_params):
    n_estimators = best_params['n_estimators']
    max_features = best_params['max_features']
    max_depth = best_params['max_depth']
    criterion = best_params['criterion']
    random_forest_classifier = RandomForestClassifier(n_estimators=n_estimators, max_features=max_features,
                                                        max_depth=max_depth, criterion=criterion)
    return random_forest_classifier
