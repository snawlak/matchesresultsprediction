from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier

from src.classifiers.common.utils import get_score, show_msg


def knn(X_train, y_train, X_test, y_test):
    classifier = KNeighborsClassifier()

    param_grid = get_param_grid()

    grid = GridSearchCV(classifier, param_grid, cv=5, scoring='accuracy')

    grid.fit(X_train, y_train)
    best_params = grid.best_params_
    train_best_score = grid.best_score_

    knn_classifier = make_classifier_with_best_params(best_params)

    test_score = get_score(knn_classifier, X_train, y_train, X_test, y_test)

    show_msg("KNN", train_best_score, test_score, best_params)


def get_param_grid():
    return {
        'n_neighbors': [53, 63, 69, 71, 73],
        'weights': ["distance"],
        'algorithm': ["auto"],
        'leaf_size': [15, 17, 19, 21, 23, 25],
        'metric': ["minkowski"],
        'p': list(range(1, 2))
    }


def make_classifier_with_best_params(best_params):
    n_neighbors = best_params['n_neighbors']
    weights = best_params['weights']
    algorithm = best_params['algorithm']
    leaf_size = best_params['leaf_size']
    metric = best_params['metric']
    p = best_params['p']

    knn_classifier = KNeighborsClassifier(n_neighbors=n_neighbors, weights=weights, algorithm=algorithm,
                                          leaf_size=leaf_size, metric=metric, p=p)
    return knn_classifier
