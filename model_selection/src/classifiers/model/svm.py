from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC

from src.classifiers.common.utils import get_score, show_msg


def svm(X_train, y_train, X_test, y_test):
    classifier = SVC()

    param_grid = get_param_grid()

    grid = GridSearchCV(classifier, param_grid, cv=5, scoring='accuracy')
    grid.fit(X_train, y_train)

    best_params = grid.best_params_
    train_best_score = grid.best_score_

    svm_classifier = make_classifier_with_best_params(best_params)

    test_score = get_score(svm_classifier, X_train, y_train, X_test, y_test)

    show_msg("SVM", train_best_score, test_score, best_params)


def get_param_grid():
    return {
        'C': [1, 2, 5],
        'kernel': ["rbf"],
        'gamma': [0.4, 0.5, 0.6, 0.9]}


def make_classifier_with_best_params(best_params):
    C = best_params['C']
    kernel = best_params['kernel']
    gamma = best_params['gamma']
    svm_classifier = SVC(C=C, kernel=kernel, gamma=gamma)
    return svm_classifier
