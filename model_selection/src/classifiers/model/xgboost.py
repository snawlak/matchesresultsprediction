from sklearn.model_selection import GridSearchCV
from xgboost import XGBClassifier

from src.classifiers.common.utils import get_score, show_msg


def xgboost(X_train, y_train, X_test, y_test):
    classifier = XGBClassifier()

    param_grid = get_param_grid()

    grid = GridSearchCV(classifier, param_grid, cv=5, scoring='accuracy')
    grid.fit(X_train, y_train)

    best_params = grid.best_params_
    train_best_score = grid.best_score_

    xgboost_classifier = make_classifier_with_best_params(best_params)

    xgboost_classifier.fit(X_train, y_train)

    test_score = get_score(xgboost_classifier, X_train, y_train, X_test, y_test)

    show_msg("xgboost", train_best_score, test_score, best_params)


def make_classifier_with_best_params(best_params):
    min_child_weight = best_params['min_child_weight']
    gamma = best_params['gamma']
    subsample = best_params['subsample']
    colsample_bytree = best_params['colsample_bytree']
    max_depth = best_params['max_depth']
    xgboost_classifier = XGBClassifier(min_child_weight=min_child_weight, gamma=gamma, subsample=subsample,
                                       colsample_bytree=colsample_bytree, max_depth=max_depth)
    return xgboost_classifier


def get_param_grid():
    return {
        'min_child_weight': [0.1, 1, 5],
        'gamma': [0.2, 0.25, 0.3],
        'subsample': [1.0],
        'colsample_bytree': [0.5, 0.6, 0.7],
        'max_depth': [1, 2, 3, 4, 5, 6]
    }
