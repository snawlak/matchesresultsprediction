from sklearn.naive_bayes import GaussianNB

from src.classifiers.common.utils import get_score, show_msg


def naive_bayes(X_train, y_train, X_test, y_test):
    nb = GaussianNB()
    test_score = get_score(nb, X_train, y_train, X_test, y_test)

    show_msg("Naive Bayes", "--", test_score, "--")
