from sklearn.model_selection import train_test_split

from src.classifiers.model.knn import knn
from src.classifiers.model.naive_bayes import naive_bayes
from src.classifiers.model.neural_network import neural_network
from src.classifiers.model.random_forrest import random_forrest
from src.classifiers.model.svm import svm
from src.classifiers.model.xgboost import xgboost
from src.data_preparation import get_data, make_results, get_x, get_y

dataset = get_data()
dataset = make_results(dataset)

X = get_x(dataset)
y = get_y(dataset)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

print("Selecting best params has begun...")

knn(X_train, y_train, X_test, y_test)
naive_bayes(X_train, y_train, X_test, y_test)
xgboost(X_train, y_train, X_test, y_test)
random_forrest(X_train, y_train, X_test, y_test)
svm(X_train, y_train, X_test, y_test)
neural_network(dataset)
