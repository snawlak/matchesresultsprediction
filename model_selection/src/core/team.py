class Team:

    def __init__(self, id, name):
        self.results = []
        self.goals_for = []
        self.goals_against = []
        self.id = id
        self.name = name

    def __str__(self) -> str:
        return "{ id: " + str(self.id) + ", name: " + self.name + " }\n"

    def add_result(self, result):
        self.results.append(result)

    def get_last_result(self):
        return self.results[-1]
