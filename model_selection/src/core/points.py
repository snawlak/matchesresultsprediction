from enum import IntEnum


class Points(IntEnum):
    WIN = 3
    DEFEAT = 0
    DRAW = 1
