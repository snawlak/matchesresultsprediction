from enum import IntEnum


class Result(IntEnum):
    HOME_WIN = 1
    AWAY_WIN = 2
    DRAW = 0
