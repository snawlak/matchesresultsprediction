class Match:

    def __init__(self, id, home_team, away_team, home_team_goals, away_team_goals):
        self.id = id
        self.home_team = home_team
        self.away_team = away_team
        self.home_team_goals = home_team_goals
        self.away_team_goals = away_team_goals

    def __str__(self) -> str:
        return "{ id: " + str(self.id) + ", home_team: " + str(self.home_team) + \
               ", away_team: " + str(self.away_team) + ", home_team_score: " + str(self.home_team_goals) + \
               ", away_team_score: " + str(self.away_team_goals) + " }\n"
