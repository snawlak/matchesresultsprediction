from django.db import models


class Match(models.Model):
    team_first_id = models.IntegerField()
    team_second_id = models.IntegerField()
    prediction_draw = models.FloatField()
    prediction_team_first_win = models.FloatField()
    prediction_team_second_win = models.FloatField()
    prediction_team_first_goals = models.IntegerField()
    prediction_team_second_goals = models.IntegerField()
