from django.http import HttpResponse
import numpy as np
from rest_framework.response import Response
from rest_framework.views import APIView

from matches_prediction_api.models import Match
from matches_prediction_api.prediction.match_prediction import predict, teach_model, predict_first_team_goals, \
    predict_second_team_goals
from matches_prediction_api.serializers import MatchSerializer


class MatchEndpoint(APIView):
    def get(self, args):
        team_first = int(self.request.query_params.get('team_first', None))
        team_second = int(self.request.query_params.get('team_second', None))

        match = Match(team_first_id=team_first, team_second_id=team_second)
        predicted_result = predict(match.team_first_id, match.team_second_id)[0]

        match.prediction_team_first_win = predicted_result[1]
        match.prediction_draw = predicted_result[0]
        match.prediction_team_second_win = predicted_result[2]

        predicted_home_goals = predict_first_team_goals(match.team_first_id, match.team_second_id)[0]
        predicted_away_goals = predict_second_team_goals(match.team_first_id, match.team_second_id)[0]

        match.prediction_team_first_goals = np.argmax(predicted_home_goals)
        match.prediction_team_second_goals = np.argmax(predicted_away_goals)

        serializer = MatchSerializer(match, many=False)
        return Response(serializer.data)


class TeachModelEndpoint(APIView):

    @staticmethod
    def get(args):
        teach_model()
        return HttpResponse('Model has been teach', status=200)
