import glob

import pandas as pd


def get_data():
    path = "resource/matches/main_leagues/*.csv"
    all_files = glob.glob(path)

    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    dataset = pd.concat(li, axis=0, ignore_index=True)
    dataset = dataset.sort_values(by=['event_timestamp'])

    return dataset.drop(
        ['fixture_id', 'league_id', 'league_name', 'league_country', 'league_logo', 'league_flag', 'event_date',
         'event_timestamp', 'firstHalfStart', 'secondHalfStart', 'round', 'status', 'statusShort', 'elapsed',
         'venue', 'referee', 'homeTeam_team_name', 'homeTeam_logo', 'awayTeam_team_name', 'awayTeam_logo',
         'score_halftime', 'score_fulltime', 'score_extratime', 'score_penalty'], axis='columns')


def make_results(dataset):
    home_team_goals = dataset['goalsHomeTeam']
    away_team_goals = dataset['goalsAwayTeam']
    results = []

    for i in range(len(home_team_goals)):
        if home_team_goals[i] > away_team_goals[i]:
            results.append(1)
        if home_team_goals[i] < away_team_goals[i]:
            results.append(2)
        if home_team_goals[i] == away_team_goals[i]:
            results.append(0)

    return dataset.join(pd.DataFrame({'result': results}))
