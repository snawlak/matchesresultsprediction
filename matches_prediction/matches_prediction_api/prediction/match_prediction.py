from pandas import DataFrame

from matches_prediction_api.prediction.data_preparation import get_data, make_results
from matches_prediction_api.prediction.model import get_xgboost_classifier

results_classifier = None
goals_first_team_classifier = None
goals_second_team_classifier = None


def predict(team_first, team_second):
    match = DataFrame([[team_first, team_second]], columns=['homeTeam_team_id', 'awayTeam_team_id'])
    predicted_result = results_classifier.predict_proba(match.iloc[:1])
    return predicted_result


def predict_first_team_goals(team_first, team_second):
    match = DataFrame([[team_first, team_second]], columns=['homeTeam_team_id', 'awayTeam_team_id'])
    predicted_result = goals_first_team_classifier.predict_proba(match.iloc[:1])
    return predicted_result


def predict_second_team_goals(team_first, team_second):
    match = DataFrame([[team_first, team_second]], columns=['homeTeam_team_id', 'awayTeam_team_id'])
    predicted_result = goals_second_team_classifier.predict_proba(match.iloc[:1])
    return predicted_result


def teach_model():
    # logger = logging.getLogger(__name__)

    # logger.info("Teaching model has been started")
    print("Teaching model has been started")
    teach_results_model()
    teach_first_team_goals_model()
    teach_away_goals_model()
    # logger.info("Teaching model has been finished")
    print("Teaching model has been finished")


def teach_results_model():
    dataset = get_data()
    dataset = make_results(dataset)

    X = dataset.drop(['goalsHomeTeam', 'goalsAwayTeam', 'result'], axis='columns')
    y = dataset['result'].values

    xgb = get_xgboost_classifier()
    xgb.fit(X, y)

    global results_classifier
    results_classifier = xgb


def teach_first_team_goals_model():
    dataset = get_data()
    X = dataset.drop(['goalsHomeTeam', 'goalsAwayTeam'], axis='columns')
    y = dataset['goalsHomeTeam'].values

    xgb = get_xgboost_classifier()
    xgb.fit(X, y)

    global goals_first_team_classifier
    goals_first_team_classifier = xgb


def teach_away_goals_model():
    dataset = get_data()
    X = dataset.drop(['goalsHomeTeam', 'goalsAwayTeam'], axis='columns')
    y = dataset['goalsAwayTeam'].values

    xgb = get_xgboost_classifier()
    xgb.fit(X, y)

    global goals_second_team_classifier
    goals_second_team_classifier = xgb
