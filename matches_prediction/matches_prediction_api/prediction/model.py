from xgboost import XGBClassifier


def get_xgboost_classifier():
    colsample_bytree = 0.6
    gamma = 0.25
    max_depth = 4
    min_child_weight = 1
    subsample = 1.0

    xgb = XGBClassifier(min_child_weight=min_child_weight, gamma=gamma, subsample=subsample,
                        colsample_bytree=colsample_bytree, max_depth=max_depth)

    return xgb
