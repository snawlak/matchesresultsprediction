from django.apps import AppConfig

from matches_prediction_api.prediction.match_prediction import teach_model


class MatchesPredictionApiConfig(AppConfig):
    name = 'matches_prediction_api'

    def ready(self):
        teach_model()
