from rest_framework import serializers

from matches_prediction_api.models import Match


class MatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Match
        fields = '__all__'
