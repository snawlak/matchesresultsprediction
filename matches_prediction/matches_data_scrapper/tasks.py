from __future__ import absolute_import, unicode_literals

import urllib.request

from celery import task

from matches_data_scrapper.scrapper.data_scrapper import fetch_matches_and_save_to_file


@task()
def data_scrapper():
    fetch_matches_and_save_to_file()

    url = 'http://127.0.0.1:8070/teach-model/'

    with urllib.request.urlopen(url, timeout=1000) as response:
        print(response.read())
