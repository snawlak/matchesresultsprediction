import csv
from pathlib import Path


class FileManager:

    def __init__(self):
        self.basic_directory = "resource/matches/"
        self.file_type = ".csv"
        self.file_mode = "a"

    def create_directory(self, dir_name):
        Path(self.basic_directory + dir_name).mkdir(parents=True, exist_ok=True)

    def create_file(self, directory, file_name, file_headers):
        file_name = self.basic_directory + directory + "/" + file_name + self.file_type

        file = csv.writer(open(file_name, self.file_mode, newline=''))
        file.writerow(file_headers)

    def is_file(self, directory, file_name):
        file_name = self.basic_directory + directory + "/" + file_name + self.file_type

        file = Path(file_name)
        if file.exists():
            return True

        return False

    def append_row(self, directory, file_name, row):
        file_name = self.basic_directory + directory + "/" + file_name + self.file_type

        file = csv.writer(open(file_name, self.file_mode, newline=''))
        file.writerow(row)

    def file_len(self, directory, file_name, ):
        file_name = self.basic_directory + directory + "/" + file_name + self.file_type
        with open(file_name) as f:
            for i, l in enumerate(f):
                pass
        return i + 1
