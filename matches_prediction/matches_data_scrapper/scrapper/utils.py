def get_league_name_if_is_main_league(country, league_name):
    if league_name == 'Bundesliga 1' and country == 'Germany':
        return league_name

    if league_name == 'Championship' and country == 'England':
        return league_name

    if league_name == 'Czech Liga' and country == 'Czech-Republic':
        return league_name

    if league_name == 'Eredivisie' and country == 'Netherlands':
        return league_name

    if league_name == 'Jupiler Pro League' and country == 'Belgium':
        return league_name

    if league_name == 'Ligue 1' and country == 'France':
        return league_name

    if league_name == 'Premier League' and country == 'England':
        return league_name

    if league_name == 'Premiership' and country == 'Scotland':
        return league_name

    if league_name == 'Primeira Liga' and country == 'Portugal':
        return league_name

    if league_name == 'Primera Division' and country == 'Spain':
        return league_name

    if league_name == 'Premier League' and country == 'Russia':
        return "RussPremier League"

    if league_name == 'Serie A' and country == 'Italy':
        return league_name

    if league_name == 'Super Lig' and country == 'Turkey':
        return league_name

    if league_name == 'Superligaen' and country == 'Denmark':
        return league_name

    if league_name == 'Tipp3 Bundesliga' and country == 'Austria':
        return league_name

    if league_name == 'Premier League' and country == 'Ukraine':
        return 'UkrPremier League'

    return None


