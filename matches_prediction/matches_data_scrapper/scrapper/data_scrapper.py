import datetime
import json
import logging
from collections import namedtuple

from matches_data_scrapper.scrapper.file_manager import FileManager
from matches_data_scrapper.scrapper.football_api_client import FootballApiClient
from matches_data_scrapper.scrapper.match_model import MatchModel
from matches_data_scrapper.scrapper.utils import get_league_name_if_is_main_league


def fetch_matches_and_save_to_file():
    client = FootballApiClient()
    match_model = MatchModel()
    file_manager = FileManager()

    fetch_match_date = get_previous_date()

    matches = fetch_matches(client, fetch_match_date)

    save_matches_to_file(file_manager, match_model, matches)

    number_of_matches_fetched = len(matches)
    total_number_of_matches_fetched = file_manager.file_len('all_leagues/', 'AllResults')

    log_info(number_of_matches_fetched, total_number_of_matches_fetched)


def get_previous_date():
    today = datetime.datetime.today().strftime('%Y-%m-%d')
    fetch_match_date = datetime.datetime.strptime(today, '%Y-%m-%d') - datetime.timedelta(days=120)
    fetch_match_date = fetch_match_date.date().__str__()
    return fetch_match_date


def save_matches_to_file(file_manager, match_model, matches):
    for match in matches:
        save_match(file_manager, match, match_model)


def save_match(file_manager, match, match_model):
    match_status = match.status

    if match_status == "Match Finished":
        directory_name = 'all_leagues/' + match.league.country
        file_name = match.league.name

        create_file_if_not_exists(directory_name, file_manager, file_name, match_model)

        append_to_files(directory_name, file_manager, file_name, match, match_model)


def append_to_files(directory_name, file_manager, file_name, match, match_model):
    append_match_to_specified_file(file_manager, directory_name, file_name, match_model, match)
    append_match_to_all_results_file(file_manager, match_model, match)
    append_match_to_main_league_if_main_league(file_name, file_name, file_manager, match_model, match)


def create_file_if_not_exists(directory_name, file_manager, file_name, match_model):
    if not file_manager.is_file(directory_name, file_name):
        file_manager.create_directory(directory_name)
        file_manager.create_file(directory_name, file_name, match_model.attributes)


def fetch_matches(client, fetch_match_date):
    response = client.fetch(fetch_match_date)

    data = json.loads(response, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    matches = data.api.fixtures
    return matches


def append_match_to_specified_file(file_manager, directory_name, file_name, match_model, match):
    file_manager.append_row(directory_name, file_name, match_model.to_row(match))


def append_match_to_all_results_file(file_manager, match_model, match):
    file_manager.append_row('all_leagues', 'AllResults', match_model.to_row(match))


def append_match_to_main_league_if_main_league(country, league_name, file_manager, match_model, match):
    main_league = get_league_name_if_is_main_league(country, league_name)

    if main_league is not None:
        file_manager.append_row('main_leagues', main_league, match_model.to_row(match))


def log_info(number_of_matches_fetched, total_number_of_matches_fetched):
    logger = logging.getLogger(__name__)
    logger.info('Number of matches fetched: ' + str(number_of_matches_fetched))
    logger.info('Total number of matches fetched: ' + str(total_number_of_matches_fetched))
