import requests


class FootballApiClient:

    def __init__(self):
        self.url = "https://api-football-v1.p.rapidapi.com/v2/fixtures/date/"
        self.headers = {
            'x-rapidapi-host': "api-football-v1.p.rapidapi.com",
            'x-rapidapi-key': "7c1cc08b2dmshab44d24617fd33cp11e386jsna6a4b1aa9dfd"
        }

    def fetch(self, date):
        url = self.url + date
        response = requests.request("GET", url, headers=self.headers)

        data = response.text
        return data
