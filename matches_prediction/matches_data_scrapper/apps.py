from django.apps import AppConfig


class MatchesDataScrapperConfig(AppConfig):
    name = 'matches_data_scrapper'
